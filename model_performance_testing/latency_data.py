# -*- coding: utf-8 -*-
"""
Created on Tue Jan 24 16:05:12 2023

@author: Philip
"""
from __future__ import division
from random import sample
from rdp import rdp
from numpy.lib.stride_tricks import as_strided as ast
import numpy as np
import matplotlib.pyplot as plt

import argparse
import multiprocessing as mp
import os
import re
import sys
from functools import reduce

from IPython.core import ultratb

low_cut = 0  # 650
high_cut = 10000

data_range = [12000, 50000]

def get_train_data(data_file_quantity, directory, data_type, window, overlap):
    #print("Getting data: " + data_type)

    if data_type == "test":
        file_low = 9000
        file_high = 9999
    elif data_type == "train":
        file_low = 0
        file_high = 8999
    else:
        print("INCORRECT FUNCTION PARAMETERS")

    data_file_indices = sample(range(file_low, file_high), data_file_quantity)
    data_list = []

    for index in data_file_indices:
        filename = directory + str(index)
        data = convolve_data_from_file(filename, window, overlap)
        data_list.append(data)

    return data_list


def chunk_data(data, window_size, overlap_size=0, flatten_inside_window=True):
    assert data.ndim == 1 or data.ndim == 2
    if data.ndim == 1:
        data = data.reshape((-1, 1))

    num_windows = (data.shape[0] -
                   window_size) // (window_size - overlap_size) + 1
    overhang = data.shape[0] - \
        (num_windows*window_size - (num_windows-1)*overlap_size)

    sz = data.dtype.itemsize
    ret = ast(
        data,
        shape=(num_windows, window_size*data.shape[1]),
        strides=((window_size-overlap_size)*data.shape[1]*sz, sz)
    )

    if flatten_inside_window:
        return ret
    else:
        return ret.reshape((num_windows, -1, data.shape[1]))


def plot_window(l, window, overlap):
    ws = chunk_data(l, window, overlap)
    w_avg = np.mean(ws, 1)
    w_std = np.std(ws, 1)
    all_avg = np.mean(w_avg)
    f_w_std = w_std#[w_std < 100]
    c_var = f_w_std  # / all_avg
    return c_var


def convolve_data_from_file(f, window, overlap):
    with open(f, 'r') as ff:
        s = ff.read()
        it = re.finditer(r'^\d.*$', s, flags=re.MULTILINE)
        l = [[int(y) for y in x.group().split()] for x in it]

        l = reduce(lambda x, y: x+y, l)
        l = l[data_range[0]:data_range[1]]
        l = np.array(l)
        if l.size == 1:
            print("uhoh! empty!")
        l_avg = int(np.mean(l))
        l[(l > high_cut) | (l < low_cut)] = l_avg
        #l = l_avg
        
        if window > 0:
            f_w_std = plot_window(l, window, overlap)
            avg_f_w_std = np.mean(f_w_std)
        else:
            print(window)

        ff.close()

    return f_w_std


#print(convolve_data_from_file("training_data/gpu_on/1", 1000, 100))
