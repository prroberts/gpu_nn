#!/usr/bin/env python
# coding: utf-8

import os
import logging
logging.disable(logging.WARNING)
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# In[1]:
import sys
import matplotlib.pyplot as plt
import graphviz
import pydot
import keras
from tensorflow.keras.metrics import binary_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle
import numpy as np
from latency_data import get_train_data
from latency_data import convolve_data_from_file


# In[2]:

print("===== PRINTING COMMAND LINE ARGUMENTS =====")
print("Layer Quantity = " + str(sys.argv[1]))
print("Layer Size = " + str(sys.argv[2]))

print("===== IMPORTING DATA =====")
data_file_num = 8999  # Max 8999
directory_on = "../training_data/gpu_on/"
directory_off = "../training_data/gpu_off/"
window = 1000
overlap = 50

samples_on = get_train_data(
    data_file_num, directory_on, "train", window, overlap)
samples_off = get_train_data(
    data_file_num, directory_off, "train", window, overlap)

print("===== FINISHED IMPORTING DATA =====")
# In[3]:


# print("===== LENGTH OF samples_on[9] is: " +
#      str(len(samples_on[9])) + " =====")


# In[4]:


samples_on

print("===== FORMATTING DATA =====")
# In[5]:


min_allowable_data_length = 0

samples_on = [i for i in samples_on if len(i) >= min_allowable_data_length]
samples_off = [i for i in samples_off if len(i) >= min_allowable_data_length]


# In[ ]:


# In[6]:


# train_samples = np.append(samples_on, samples_off)


# In[7]:


min_data_length = 1000
for i in range(len(samples_on)):
    min_data_length = len(samples_on[i]) if len(
        samples_on[i]) < min_data_length else min_data_length
for i in range(len(samples_off)):
    min_data_length = len(samples_off[i]) if len(
        samples_off[i]) < min_data_length else min_data_length
print(min_data_length)


# In[ ]:


# In[8]:


train_samples = samples_on[0][:min_data_length]
for i in range(1, len(samples_on)):
    train_samples = np.vstack(
        [train_samples, np.array(samples_on[i][:min_data_length])])
for i in range(0, len(samples_off)):
    train_samples = np.vstack(
        [train_samples, np.array(samples_off[i][:min_data_length])])


# In[ ]:


# In[9]:


train_labels = np.append(np.ones(len(samples_off)), np.zeros(len(samples_on)))


# In[10]:


# print(train_samples.shape)
# print(train_labels.shape)


# In[11]:


x_train = train_samples
y_train = train_labels


# In[12]:


#print(x_train[x_train != 0.0])


# In[13]:


classes = np.unique(y_train)

plt.figure()
for c in classes:
    c_x_train = x_train[y_train == c]
    plt.plot(c_x_train[0], label="class " + str(c))
plt.legend(loc="best")
plt.show()
plt.close()


# In[14]:


x_train = x_train.reshape((x_train.shape[0], x_train.shape[1], 1))
# x_train.shape


# In[15]:


num_classes = len(np.unique(y_train))
# num_classes


# In[16]:


idx = np.random.permutation(len(x_train))


# In[17]:


# x_train = x_train[idx]
# y_train = y_train[idx]


# In[23]:

print("===== FINISHED FORMATTING DATA =====")
print("===== CREATING MODEL =====")


def make_model(input_shape):
    input_layer = keras.layers.Input(input_shape)
    dense = None
    for i in range(int(sys.argv[1])):
        print("i is )" + str(i) +
              " and adding dense with neuron count " + str(sys.argv[2]))
        if i == int(sys.argv[1]) - 1:
            layer_size = 2
        else:
            layer_size = int(sys.argv[2])
        if dense is None:
            dense = keras.layers.Dense(layer_size)(input_layer)
        else:
            dense = keras.layers.Dense(layer_size)(dense)

    if dense is None:
        gap = keras.layers.GlobalAveragePooling1D()(input_layer)
    else:
        gap = keras.layers.GlobalAveragePooling1D()(dense)

    output_layer = keras.layers.Dense(num_classes, activation="softmax")(gap)

    return keras.models.Model(inputs=input_layer, outputs=output_layer)


model = make_model(input_shape=x_train.shape[1:])
keras.utils.plot_model(model, show_shapes=True)


# In[24]:


model.summary()

print("===== TRAINING MODEL =====")
# In[25]:


epochs = 500
batch_size = 10

callbacks = [
    keras.callbacks.ModelCheckpoint(
        "best_model.h5", save_best_only=True, monitor="val_loss"
    ),
    keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss", factor=0.5, patience=20, min_lr=0.000001
    ),
    keras.callbacks.EarlyStopping(monitor="val_loss", patience=50, verbose=1),
]
model.compile(
    optimizer="adam",
    loss="sparse_categorical_crossentropy",
    metrics=["sparse_categorical_accuracy"],
)
history = model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=epochs,
    callbacks=callbacks,
    validation_split=0.2,
    verbose=1,
)
