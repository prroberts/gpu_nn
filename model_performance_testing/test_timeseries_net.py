#!/usr/bin/env python
# coding: utf-8


import logging
import os
logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"
# In[1]:
import sys
import matplotlib.pyplot as plt
import graphviz
import pydot
import keras
from tensorflow.keras.metrics import binary_crossentropy
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.layers import Dense
from tensorflow.keras.models import Sequential
from sklearn.preprocessing import MinMaxScaler
from sklearn.utils import shuffle
import numpy as np
from latency_data import get_train_data
from latency_data import convolve_data_from_file


# In[2]:


data_file_num = 999  # Max 999
directory_on = "../testing_data/gpu_on/"
directory_off = "../testing_data/gpu_off/"
window = 1000
overlap = 50

samples_on = get_train_data(
    data_file_num, directory_on, "test", window, overlap)
samples_off = get_train_data(
    data_file_num, directory_off, "test", window, overlap)


# In[10]:


min_allowable_data_length = 0

samples_on = [i for i in samples_on if len(i) >= min_allowable_data_length]
samples_off = [i for i in samples_off if len(i) >= min_allowable_data_length]


# In[11]:


min_data_length = 1000
for i in range(len(samples_on)):
    min_data_length = len(samples_on[i]) if len(
        samples_on[i]) < min_data_length else min_data_length
for i in range(len(samples_off)):
    min_data_length = len(samples_off[i]) if len(
        samples_off[i]) < min_data_length else min_data_length

test_samples = samples_on[0][:min_data_length]
for i in range(1, len(samples_on)):
    test_samples = np.vstack(
        [test_samples, np.array(samples_on[i][:min_data_length])])
for i in range(0, len(samples_off)):
    test_samples = np.vstack(
        [test_samples, np.array(samples_off[i][:min_data_length])])


# In[12]:


test_labels = np.append(np.ones(len(samples_off)), np.zeros(len(samples_on)))


# In[13]:


x_test = test_samples
y_test = test_labels
x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], 1))
idx = np.random.permutation(len(x_test))
x_test = x_test[idx]
y_test = y_test[idx]


# In[14]:


model = keras.models.load_model("best_model.h5")


# In[15]:


predictions = model.predict(x=x_test, verbose=2, batch_size=1).transpose()[0]


# In[16]:


rounding_digits = 7
accuracy = 0

for i in range(len(predictions)):
    accuracy = accuracy + abs(predictions[i] - y_test[i])

print(accuracy / (i + 1))


# In[ ]:


# In[ ]:
