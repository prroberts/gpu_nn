#!/bin/bash

echo "layers,layer size,accuracy" >> output.txt

for i in {1..10}
do
	for j in {2..32..2}
	do
		rm -f best_model.h5
		python3 train_timeseries_net.py $i $j
		accuracy=$(python3 test_timeseries_net.py &)
		accuracy2=$(echo $accuracy | cut -d " " -f 8)
		echo "======= BASH SCRIPT: $i, $j ======="
		echo "======= BASH SCRIPT: $accuracy2 ======="
		echo "$i,$j,$accuracy2" >> output.txt
	done
done
