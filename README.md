# Neural Network for GPU Usage Detection

## How to use
To train the network, first place the corresponding data files in the gpu_on and gpu_off subdirectories of training_data. To test the network, place the validation data files in the gpu_on and gpu_off subdirectories of training_data.

<span><pre>
.
├── README.md
├── convolution.py
├── math_approx.py
├── sig_plot_all.py
├── train_net.py
└── training_data
&emsp;├── gpu_off
&emsp;├── gpu_on
&emsp;└── rdrand_lat.zip
</pre></span>

