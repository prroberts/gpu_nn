import numpy as np
from random import randint
from sklearn.utils import shuffle
from sklearn.preprocessing import MinMaxScaler

from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
import math

# Function: y = sqrt(x)


def math_function(x):
    y = math.sqrt(x)
    return y


# Generate the data
train_samples = []
train_labels = []

for i in range(1000):
    random_x = randint(0, 100000)
    random_y = math_function(random_x)

    train_samples.append(random_x)
    train_labels.append(random_y)

# Convert python array of data to numpy array
train_samples = np.array(train_samples)
train_labels = np.array(train_labels)

train_samples, train_labels = shuffle(train_samples, train_labels)
scaler = MinMaxScaler(feature_range=(0, 1))
scaled_train_samples = scaler.fit_transform(train_samples.reshape(-1, 1))
scaled_train_labels = scaler.fit_transform(train_labels.reshape(-1, 1))

# Create Neural Network
model = Sequential([
    Dense(units=8, input_shape=(1,), activation='relu'),
    Dense(units=16, activation='relu'),
    Dense(units=1, activation='sigmoid'),
])


def loss_function(y_true, y_pred):
    loss = abs(y_true - y_pred)
    return loss

model.summary()

model.compile(optimizer=Adam(learning_rate=0.0001),
              loss=loss_function, metrics=['accuracy'])
model.fit(x=scaled_train_samples, y=scaled_train_labels,
          validation_split=0.1, batch_size=10, epochs=30, shuffle=True, verbose=2)

# Generate Test Data
test_samples = []
test_labels = []

for i in range(1000):
    random_x = randint(0, 100000)
    random_y = math_function(random_x)

    test_samples.append(random_x)
    test_labels.append(random_y)

# Convert python array of data to numpy array
test_samples = np.array(train_samples)
test_labels = np.array(train_labels)
test_samples, test_labels = shuffle(test_samples, test_labels)

scaler = MinMaxScaler(feature_range=(0, 1))
scaled_test_samples = scaler.fit_transform(test_samples.reshape(-1, 1))
scaled_test_labels = scaler.fit_transform(test_labels.reshape(-1, 1))

predictions = model.predict(x=scaled_test_samples, batch_size=10, verbose=0)
sum = 0
for i in range(len(predictions)):
    sum = sum + (1 - abs(scaled_test_labels[i] - predictions[i]))

print(sum / len(predictions))
